<?php

/**
 * @file
 * Contains \Drupal\fbphotosync\Form\FbphotosyncSettingsForm
 */
namespace Drupal\fbphotosync\Form;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class FbphotosyncSettingsForm extends ConfigFormBase {

  public function getFormId() {
    return 'fbphotosync_form';
  }  
  
  public function buildForm(array $form, array &$form_state, $reset = FALSE) {
   $config = \Drupal::config('fbphotosync.settings');
   $fbphotosync_settings = $config->get('fbphotosync_settings');
   
   if ($fbphotosync_settings) {
     $settings = $fbphotosync_settings;    
   }
   
   //$settings = isset($form_state['input']['fbphotosync_settings'])
   // ? $form_state['input']['fbphotosync_settings']
   // : $fbphotosync_settings;
   //$settings = $fbphotosync_settings;
   
     if ($reset == 'reset') {
    $loginurl = fbphotosync_userauthorization($fbphotosync_settings['app_id'], $fbphotosync_settings['app_secret'],true);
    $response = new RedirectResponse($loginurl);
    return $response;
  }
   
   
  $form['fbphotosync_settings'] = array(
    '#tree' => TRUE,
  );

  $form['fbphotosync_settings']['app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#default_value' => isset($settings['app_id']) ? $settings['app_id'] : '',
    '#required' => TRUE,
  );

  $form['fbphotosync_settings']['app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App secret'),
    '#default_value' => isset($settings['app_secret']) ? $settings['app_secret'] : '',
    '#required' => TRUE,
  );
  
  $form['fbphotosync_settings']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save'
  );

   if ((\Drupal::request()->getMethod()  == "GET") && (\Drupal::request()->get('code'))) {
     $appid = $_SESSION['fbphotosync_appid'];
     $appsecret = $_SESSION['fbphotosync_appsecret'];
     $accesstoken = fbphotosync_get_access_token($appid,$appsecret);  
     $settings['app_id'] = $appid;
     $settings['app_secret'] = $appsecret;
     $settings['access_token'] = $accesstoken;
     $incorrectaccesstoken="{$appid}|{$appsecret}";
     
     if ($accesstoken == $incorrectaccesstoken) {
       $this->setFormError('', $form_state, 'Invalid access token returned');    
     }    
      
     $config->set('fbphotosync_settings',$settings);
     
     $form['fbphotosync_settings']['access_token'] = array(
       '#type' => 'value',
       '#value' => $accesstoken,
     );
      
     $form['fbphotosync_settings']['authenticated'] = array(
       '#type' => 'fieldset',
       '#title' => t('Facebook User'),
       '#theme' => 'fbphotosync_authenticated_user',
  );
     
     return $form;
  }
  
  if (empty($form_state['input']['fbphotosync_settings']['app_id'])) {
    if ($fbphotosync_settings) {
      $appid = $fbphotosync_settings['app_id'];
      $appsecret = $fbphotosync_settings['app_secret'];
      $accesstoken = $fbphotosync_settings['access_token'];
      
      $incorrectaccesstoken="{$appid}|{$appsecret}";
     
      if ($accesstoken == $incorrectaccesstoken) {
       $this->setFormError('', $form_state, 'Invalid access token returned');    
      }
      
      else {
      fbphotosync_show_facebook_credentials($form,$form_state,$accesstoken);
      }
    }
    
    return $form;
  }
  
  
 
  
  
  if (\Drupal::request()->getMethod()  == "POST") {
    $isValidCredentials = fbphotosync_isAppCredentialsValid($form_state['input']['fbphotosync_settings']['app_id'], $form_state['input']['fbphotosync_settings']['app_secret']);
  
    if (!$isValidCredentials) {
      $this->setFormError('', $form_state, $this->t('App ID or App secret not valid'));
      return $form;
    }
  }
  
  
  
  

  
  
  if (!isset($_SESSION['appid'])) {
    $_SESSION['fbphotosync_appid'] = $form_state['input']['fbphotosync_settings']['app_id'];
    $_SESSION['fbphotosync_appsecret'] = $form_state['input']['fbphotosync_settings']['app_secret'];
    $loginurl = fbphotosync_userauthorization($form_state['input']['fbphotosync_settings']['app_id'], $form_state['input']['fbphotosync_settings']['app_secret'],false);
    $response = new RedirectResponse($loginurl);
    
    return $response;
  }
  
  else
  {
      $access_token = fbphotosync_get_access_token($appid, $app);
  }
  
  $incorrectaccesstoken="{$appid}|{$appsecret}";
     
  if ($accesstoken == $incorrectaccesstoken) {
       $this->setFormError('', $form_state, 'Invalid access token returned');    
  } 
  
  $form['fbphotosync_settings']['access_token'] = array(
    '#type' => 'value',
    '#value' => $settings['access_token'],
  );
      
  $form['fbphotosync_settings']['authenticated'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook User'),
    '#theme' => 'fbphotosync_authenticated_user',
  );
  
  /*
  //compare appid &secret from settings with appid & settings from form, if different skip
  if ((isset($settings['access_token']) && fbphotosync_facebook_sdk_is_authenticated($facebook, $settings['access_token']))) {
     unset($_SESSION[md5(serialize($settings))]); 
         if (!empty($settings)) {
      $form['fbphotosync_settings']['access_token'] = array(
        '#type' => 'value',
        '#value' => $settings['access_token'],
      );
      
      $form['fbphotosync_settings']['authenticated'] = array(
        '#type' => 'fieldset',
        '#title' => t('Facebook User'),
        '#theme' => 'fbphotosync_authenticated_user',
      );
      
    }
  
     

      return $form;
  }
  
  
  if (!isset($settings['access_token']) || (isset($settings['access_token']) && !fbphotosync_facebook_sdk_is_authenticated($facebook, $settings['access_token']))) {
    
      if (isset($_GET['code'])) {
       
      $settings['access_token'] = $facebook->getAccessToken();
      $config->set('fbphotosync_settings', $settings);
      $config->save();
      return new RedirectResponse('/admin/config/services/fbphotosync');       
  }  
   
  else {
      unset($settings['access_token']);
      $config->set('fbphotosync_settings', $settings);
      $config->save();
      $md5 = md5(serialize($settings));
    if (!isset($_SESSION[$md5])) {
      $_SESSION[md5(serialize($settings))] = FALSE;
      return new RedirectResponse($facebook->getLoginUrl(array('scope' => array('manage_pages', 'offline_access', 'publish_stream', 'user_photos'))));
    }
    
    else {
      unset($_SESSION[md5(serialize($settings))]);
      \Drupal::formBuilder()->setErrorByName('fbphotosync_settings', $form_state, 'Authentication failed');
      return $form;     
    }
    
  } 
    
  }
  
  

  

      
      
     

    if (!empty($settings)) {
      $form['fbphotosync_settings']['access_token'] = array(
        '#type' => 'value',
        '#value' => $settings['access_token'],
      );
      
      $form['fbphotosync_settings']['authenticated'] = array(
        '#type' => 'fieldset',
        '#title' => t('Facebook User'),
        '#theme' => 'fbphotosync_authenticated_user',
      );
      
    }
  
     
*/
      return $form;
  }
 
  public function validateForm(array &$form, array &$form_state) {
      parent::validateForm($form, $form_state);
  }
  
  public function submitForm(array &$form, array &$form_state) {
      parent::submitForm($form, $form_state);
  }
  
}

